# pam module for irods authentication to sram

This directory contains pam-validate.py file that is used to authenticate irods users to sram.

## Requirements

In order to use this module you should install pam_python package.
https://sourceforge.net/projects/pam-python/


## Setting up pam module

You should add a pam stack for irods. This is file named "irods" in path "/etc/pam.d/irods"

The content of this file should look like this:
```
auth required /usr/lib/security/pam_python.so /usr/local/bin/pam-validate.py debug url=https://sram.surf.nl token=XXXX
```

The token mentioned in this file is provided by sram by the name "User Introspection Token"

make sure /usr/local/bin/pam-validate.py is accessible by irods user.
